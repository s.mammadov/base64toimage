/** @format */

const button = document.querySelector("#getDataButton");

function getInputValue() {
  return document.querySelector("input").value;
}

function downloadBase64File(ImageBase64) {
  var a = document.createElement("a"); //Create <a>
  a.href = "data:image/png;base64," + ImageBase64; //Image Base64 Goes here
  a.download = "Image.png"; //File name Here
  a.click(); //Downloaded file
}

button.addEventListener("click", async () => {
  const userInput = getInputValue();
  console.log(userInput);
  const data = await fetch("http://localhost:3000/getData", {
    method: "POST", // or 'PUT'
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ value: userInput }),
  });
  const dataJson = await data.json();
  const base64Value = dataJson.proof.split(",")[1];
  downloadBase64File(base64Value);
});
