/** @format */

import express from "express";
import fetch from "node-fetch";
import path from "path";
// const fetch = require("node-fetch");
const app = express();
const port = 3000;
const __dirname = path.resolve();

app.use(express.json());

//Routing//
app.get("/", app.use(express.static(path.join(__dirname, "./public"))));

app.post("/getData", async (req, res) => {
  const data = await fetch(
    `https://ocapiqa.jobtarget.com:8080/api/proof/GetPostingProof/${req.body.value}`
  );
  const dataJson = await data.json();
  res.send(dataJson);
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
